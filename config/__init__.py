import os

USING_DOCKER = os.environ.get('USING_DOCKER')
PER_PAGE = 20
PROJECT_NAME = 'complex_project'
