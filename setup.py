def setup_test_users():
    from database.users import User, ACCESS_LEVEL
    from hashlib import md5

    users = {
        'simple_user': ['qwerty12345', ACCESS_LEVEL.USER],
        'admin_user': ['high_entropy_password', ACCESS_LEVEL.ADMIN]
    }

    for login, pal in users.items():
        user = User.objects(username=login).first()
        if user is not None:
            if user.password_hash != md5(pal[0].encode()).hexdigest() or \
                    user.access_level != pal[1]:
                user.password_hash = md5(pal[0].encode()).hexdigest()
                user.access_level = pal[1]
                user.save()
            continue
        user = User(
            username=login,
            password_hash=md5(pal[0].encode()).hexdigest(),
            access_level=pal[1]
        )
        user.save()


def setup_test_companies():
    from database.companies import Company, Activity

    companies = {
        'Test company LTD.': [
            'https://images.com/somelogo.jpg',
            [
                'First work',
                'Second work',
                'Third work'
            ],
            'Mars. Gale crater',
            [
                [
                    'Mobile development',
                    99.99,
                    [
                        'Kotlin',
                        'Java'
                    ]
                ],
                [
                    'Web development',
                    199.99,
                    [
                        'HTML',
                        'CSS',
                        'JS',
                        'Angular',
                        'React'
                    ]
                ]
            ]
        ]
    }

    for company_name, data in companies.items():
        company = Company.objects(name=company_name).first()
        if company is not None:
            continue
        company = Company(
            name=company_name,
            logo_url=data[0],
            works=data[1],
            location=data[2]
        )
        for activity_data in data[3]:
            activity = Activity(
                name=activity_data[0],
                minimum_price=activity_data[1],
                technology_stack=activity_data[2]
            )
            activity.save()
            company.activities.append(activity)
        company.save()


if __name__ == '__main__':
    setup_test_users()
    setup_test_companies()
