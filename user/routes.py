from admin.helpers import requires_user_login
from flask import render_template, request
from database.users import User, ACCESS_LEVEL
from database.companies import Company, Activity
from helpers import is_digit
from math import ceil
from config import PER_PAGE


def register_routes(app, socketio):

    @app.route('/index')
    @app.route('/index/<page>')
    @requires_user_login
    def user_index(page=None):
        if page is None:
            page = 1
        elif is_digit(page):
            page = int(page)
        else:
            return 'Bad request', 400

        companies_count = Company.objects().count()
        start_from = page * PER_PAGE - 1

        if start_from > companies_count:
            start_from = 0
            page = 1

        end_at = (start_from + PER_PAGE) if companies_count > (start_from + PER_PAGE) else companies_count

        data = []
        for company in Company.objects()[start_from:end_at]:
            data.append({
                'id': company.company_id,
                'name': company.name,
                'location': company.location,
                'logo': company.logo_url,
                'works': ', '.join(company.works),
            })

        session_id = request.cookies.get('session_id', None)
        user = User.objects(session_id=session_id).first()

        return render_template(
            'user.html',
            data=data,
            page=page,
            showed=(end_at - start_from),
            total=companies_count,
            totalp=ceil(companies_count / (end_at - start_from)),
            is_admin=(user.access_level == ACCESS_LEVEL.ADMIN)
        )

    @app.route('/activities')
    @app.route('/activities/<page>')
    @requires_user_login
    def user_activities(page=None):
        if page is None:
            page = 1
        elif is_digit(page):
            page = int(page)
        else:
            return 'Bad request', 400

        activities_count = Activity.objects().count()
        start_from = page * PER_PAGE - 1

        if start_from > activities_count:
            start_from = 0
            page = 1

        end_at = (start_from + PER_PAGE) if activities_count > (start_from + PER_PAGE) else activities_count

        data = []
        for activity in Activity.objects()[start_from:end_at]:
            company = Company.objects(activities=activity).first()
            if company is not None:
                fc = company.name
            else:
                fc = ''
            data.append({
                'for_company': fc,
                'id': activity.work_id,
                'name': activity.name,
                'minimum_price': activity.minimum_price,
                'stack': ', '.join(activity.technology_stack),
            })

        session_id = request.cookies.get('session_id', None)
        user = User.objects(session_id=session_id).first()

        return render_template(
            'user_activities.html',
            data=data,
            page=page,
            showed=(end_at - start_from),
            total=activities_count,
            totalp=ceil(activities_count / (end_at - start_from)),
            is_admin=(user.access_level == ACCESS_LEVEL.ADMIN)
        )

    @app.route('/rules')
    @requires_user_login
    def rules():
        session_id = request.cookies.get('session_id', None)
        user = User.objects(session_id=session_id).first()
        return render_template('user_rules.html', is_admin=(user.access_level == ACCESS_LEVEL.ADMIN))

    @app.route('/search')
    @requires_user_login
    def search():
        session_id = request.cookies.get('session_id', None)
        user = User.objects(session_id=session_id).first()
        return render_template('user_search.html', is_admin=(user.access_level == ACCESS_LEVEL.ADMIN))

    @socketio.on('search_result_request')
    def search_request(data):
        from mongoengine.queryset.visitor import Q
        search_str = data.get('data')
        if search_str is None:
            return
        data = []
        for company in Company.objects(Q(name__icontains=search_str)|Q(location__icontains=search_str))[0:PER_PAGE+1]:
            data.append({
                'name': company.name,
                'location': company.location,
                'logo': company.logo_url,
                'works': ''.join(company.works)
            })
        socketio.emit('search_result_response', data)
