from admin.helpers import requires_admin_login
from flask import render_template, request, redirect, url_for
from database.companies import Company, Activity
from config import PER_PAGE
from math import ceil
from helpers import is_digit


def register_routes(app, socketio):

    @app.route('/data/create', methods=["GET", "POST"])
    @requires_admin_login
    def admin_crud_c():
        if request.method == "POST":
            name = request.form.get('name')
            location = request.form.get('location')
            logo = request.form.get('logo')
            works = []
            for k, v in request.form.items():
                if k.find('work') == 0:
                    works.append(v)
            new_company = Company(
                name=name,
                location=location,
                logo_url=logo,
                works=works
            )
            new_company.save()

            socketio.emit('company_created', new_company.name)

            return redirect(url_for('admin_index'))
        return render_template('admin_crud_creation.html')

    @app.route('/data/edit', methods=["GET", "POST"])
    @requires_admin_login
    def admin_crud_u():
        company_id = request.args.get('id')
        if company_id is None:
            return 'Bad request', 400

        company = Company.objects(company_id=company_id).first()
        if company is None:
            return 'Not found', 404

        if request.method == "POST":
            company.name = request.form.get('name')
            company.location = request.form.get('location')
            company.logo = request.form.get('logo')
            works = []
            for k, v in request.form.items():
                if k.find('work') == 0:
                    works.append(v)
            company.works = works
            company.save()

            socketio.emit('company_updated', company.name)

            return redirect(url_for('admin_index'))
        else:
            data = {
                'name': company.name,
                'location': company.location,
                'logo': company.logo_url,
                'works': company.works
            }
            return render_template('admin_crud_edit.html', **data)

    @app.route('/data/delete', methods=["POST"])
    @requires_admin_login
    def admin_crud_d():
        company_ids = request.form.get('cid')
        if company_ids is None:
            return 'Bad request', 400
        companies = company_ids.split(' ')
        for company_id in companies:
            if is_digit(company_id):
                company = Company.objects(company_id=company_id).first()
                if company:
                    socketio.emit('company_deleted', company.name)
                    company.delete()
            else:
                return 'Bad request', 400
        return 'ok'

    @app.route('/data')
    @app.route('/data/<page>')
    @requires_admin_login
    def admin_index(page=None):
        if page is None:
            page = 1
        elif is_digit(page):
            page = int(page)
        else:
            return 'Bad request', 400

        companies_count = Company.objects().count()
        start_from = page * PER_PAGE - 1

        if start_from > companies_count:
            start_from = 0
            page = 1

        end_at = (start_from + PER_PAGE) if companies_count > (start_from + PER_PAGE) else companies_count

        data = []
        for company in Company.objects()[start_from:end_at]:
            data.append({
                'id': company.company_id,
                'name': company.name,
                'location': company.location,
                'logo': company.logo_url,
                'works': ', '.join(company.works),
            })
        return render_template(
            'admin.html',
            data=data,
            page=page,
            showed=(end_at - start_from),
            total=companies_count,
            totalp=ceil(companies_count/(end_at - start_from))
        )

    @app.route('/data_activities/create', methods=["GET", "POST"])
    @requires_admin_login
    def admin_data_activities_c():
        if request.method == "POST":
            name = request.form.get('name')
            minimum_price = float(request.form.get('minprice', 0))
            stack = []
            for k, v in request.form.items():
                if k.find('stack') == 0:
                    stack.append(v)
            new_activity = Activity(
                name=name,
                minimum_price=minimum_price,
                technology_stack=stack,
            )
            new_activity.save()

            socketio.emit('activity_created', new_activity.name)

            return redirect(url_for('admin_data_activities'))
        return render_template('admin_activities_creation.html')

    @app.route('/data_activities/edit', methods=["GET", "POST"])
    @requires_admin_login
    def admin_data_activities_u():
        activity_id = request.args.get('id')
        if activity_id is None:
            return 'Bad request', 400

        activity = Activity.objects(work_id=activity_id).first()
        if activity is None:
            return 'Not found', 404

        if request.method == "POST":
            activity.name = request.form.get('name')
            activity.minimum_price = float(request.form.get('minprice', 0))
            stack = []
            for k, v in request.form.items():
                if k.find('stack') == 0:
                    stack.append(v)
            activity.technology_stack = stack
            activity.save()

            socketio.emit('activity_updated', activity.name)

            return redirect(url_for('admin_data_activities'))
        else:
            data = {
                'for_company': (Company.objects(activities=activity).first().name if Company.objects(
                    activities=activity).count != 0 else ''),
                'name': activity.name,
                'minprice': activity.minimum_price,
                'stack': activity.technology_stack
            }
            return render_template('admin_activities_edit.html', **data)

    @app.route('/data_activities/delete', methods=["POST"])
    @requires_admin_login
    def admin_data_activities_d():
        activity_ids = request.form.get('cid')
        if activity_ids is None:
            return 'Bad request', 400
        activities = activity_ids.split(' ')
        for activity_id in activities:
            if is_digit(activity_id):
                activity = Activity.objects(work_id=activity_id).first()
                if activity:
                    socketio.emit('activity_deleted', activity.name)
                    activity.delete()
            else:
                return 'Bad request', 400
        return 'ok'

    @app.route('/data_activities')
    @app.route('/data_activities/<page>')
    @requires_admin_login
    def admin_data_activities(page=None):
        if page is None:
            page = 1
        elif is_digit(page):
            page = int(page)
        else:
            return 'Bad request', 400

        activities_count = Activity.objects().count()
        start_from = page * PER_PAGE - 1

        if start_from > activities_count:
            start_from = 0
            page = 1

        end_at = (start_from + PER_PAGE) if activities_count > (start_from + PER_PAGE) else activities_count

        data = []
        for activity in Activity.objects()[start_from:end_at]:
            company = Company.objects(activities=activity).first()
            if company is not None:
                fc = company.name
            else:
                fc = ''
            data.append({
                'for_company': fc,
                'id': activity.work_id,
                'name': activity.name,
                'minimum_price': activity.minimum_price,
                'stack': ', '.join(activity.technology_stack),
            })
        return render_template(
            'admin_activities.html',
            data=data,
            page=page,
            showed=(end_at - start_from),
            total=activities_count,
            totalp=ceil(activities_count / (end_at - start_from))
        )
