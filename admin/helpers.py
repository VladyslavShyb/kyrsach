from functools import wraps
from flask import request, redirect, url_for
from database.users import User, ACCESS_LEVEL
from enum import IntEnum


class AUTH_STATUS(IntEnum):
    NO_AUTH = 0
    AUTH_NOT_AUTHORIZED = 1
    AUTH_AUTHORIZED = 2


def auth_check(level=ACCESS_LEVEL.ADMIN):
    result = AUTH_STATUS.NO_AUTH

    session_id = request.cookies.get('session_id', None)
    if session_id:
        user = User.objects(session_id=session_id).first()
        if user:
            if user.access_level >= level or level==ACCESS_LEVEL.USER:
                result = AUTH_STATUS.AUTH_AUTHORIZED
            else:
                result = AUTH_STATUS.AUTH_NOT_AUTHORIZED

    return result


def requires_admin_login(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        logged = auth_check()
        if logged == AUTH_STATUS.NO_AUTH:
            return redirect(url_for('login'))
        elif logged == AUTH_STATUS.AUTH_NOT_AUTHORIZED:
            return redirect(url_for('user_index'))
        return f(*args, **kwargs)

    return wrapper


def requires_user_login(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        logged = auth_check(ACCESS_LEVEL.USER)
        if logged == AUTH_STATUS.NO_AUTH:
            return redirect(url_for('login'))
        return f(*args, **kwargs)

    return wrapper
