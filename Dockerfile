FROM python:3.6-alpine

RUN apk --update add --no-cache gcc musl-dev linux-headers build-base python-dev py-pip jpeg-dev zlib-dev
RUN adduser -D complexproject

WORKDIR /home/complexproject

COPY requirements.txt requirements.txt
RUN python -m venv venv
RUN venv/bin/pip install -U setuptools
RUN venv/bin/pip install -r requirements.txt

COPY admin admin
COPY config config
COPY database database
COPY logs logs
COPY static static
COPY templates templates
COPY user user
COPY helpers.py root_dir.py server.py setup.py ./

RUN chown -R complexproject:complexproject ./
USER complexproject

ENV USING_DOCKER 1
ENV PYTHONUNBUFFERED 1
EXPOSE 5000
CMD venv/bin/python server.py
