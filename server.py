import eventlet
eventlet.monkey_patch()

from flask import Flask, render_template, make_response, request, redirect, url_for
from database.users import User, ACCESS_LEVEL
from flask_socketio import SocketIO
from admin.helpers import auth_check, AUTH_STATUS
from admin.routes import register_routes as register_admin_routes
from user.routes import register_routes as register_user_routes
import hashlib
import random
import time


app = Flask(__name__)
app.config['SECRET_KEY'] = 'some_secret_key'


@app.route('/')
def default():
    return redirect(url_for('login'))


@app.route('/login', methods=['GET', 'POST'])
def login():
    result = make_response(render_template('login.html'))

    if request.method == 'POST':
        user_login = request.form['username']
        password = request.form['password']

        user = User.objects(username=user_login).first()

        if user and user.password_hash == hashlib.md5(password.encode('utf-8')).hexdigest():
            hash_str = (user_login + password + str(random.randrange(111, 999))).encode('utf-8')
            hashed_pass = hashlib.md5(hash_str).hexdigest()
            session_id = hashed_pass
            time_alive = time.time() + (86400 * 7)

            user.session_id = session_id
            user.time_alive = time_alive
            user.save()

            if user.access_level == ACCESS_LEVEL.USER:
                result = make_response(redirect(url_for('user_index')))
            elif user.access_level == ACCESS_LEVEL.ADMIN:
                result = make_response(redirect(url_for('admin_index')))
            result.set_cookie('session_id', session_id)
        else:
            error = 'Пользователь с таким логином/паролем не найден!'
            result = make_response(render_template('login.html', error=error))
    else:
        auth_result = auth_check()
        if auth_result == AUTH_STATUS.AUTH_NOT_AUTHORIZED:
            result = make_response(redirect(url_for('user_index')))
        elif auth_result == AUTH_STATUS.AUTH_AUTHORIZED:
            result = make_response(redirect(url_for('admin_index')))

    return result


@app.route('/logout')
def logout():
    result = make_response(redirect('login'))

    session_id = request.cookies.get('session_id', None)
    if session_id:
        result.set_cookie('session_id', 'deleted', expires=time.time()-100)

    return result


socketio = SocketIO(app)
register_admin_routes(app, socketio)
register_user_routes(app, socketio)


@socketio.on('connect')
def handle_connection():
    print('Client connected')


@socketio.on('my event')
def handle_my_event(data):
    print(data['data'])


if __name__ == '__main__':
    from setup import setup_test_users, setup_test_companies
    from config import USING_DOCKER
    setup_test_users()
    setup_test_companies()
    if USING_DOCKER:
        socketio.run(app, host='0.0.0.0', port=5000, debug=True)
    else:
        socketio.run(app, port=5000, log_output=True)
