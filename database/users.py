from mongoengine import *
from enum import IntEnum


class ACCESS_LEVEL(IntEnum):
    USER = 0
    ADMIN = 1


class User(Document):
    user_id = SequenceField()
    username = StringField()
    password_hash = StringField()
    access_level = IntField()
    session_id = StringField()
