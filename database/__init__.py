from mongoengine import connect
from config import PROJECT_NAME, USING_DOCKER

if USING_DOCKER:
    connect(PROJECT_NAME, host='mongo')
else:
    connect(PROJECT_NAME)
