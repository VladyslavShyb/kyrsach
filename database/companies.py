from mongoengine import *


class Activity(Document):
    work_id = SequenceField()
    name = StringField()
    minimum_price = FloatField()
    technology_stack = ListField(StringField())


class Company(Document):
    company_id = SequenceField()
    name = StringField()
    logo_url = StringField()
    works = ListField(StringField())
    location = StringField()
    activities = ListField(ReferenceField(Activity, reverse_delete_rule=NULLIFY))
